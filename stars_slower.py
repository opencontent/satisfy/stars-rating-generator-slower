#!/usr/bin/env python
from datetime import datetime

import json
import os
import random
import requests
import time
import numpy as np

from random import choices

HOST_URI=os.getenv('SATISFY_HOST_URI', default='http://0.0.0.0:8080')

def send_rating(sleep_min, sleep_max, rating_value, tenant, entrypoint, title, uri, user_agent):
    rating_value_weights = list(np.random.uniform(0,1,5))
    user_agent_weights = list(np.random.uniform(0,1,5))

    sleep_time = random.randint(sleep_min, sleep_max)
    print('Sleeping', sleep_time, 'seconds')
    time.sleep(sleep_time)
    obj = {
        "version": 1, 
        "value": choices(rating_value, rating_value_weights)[0], 
        "timestamp": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ"), 
        "entrypoint-name": entrypoint, 
        "tenant-name": tenant, 
        "meta": {
            "uri": uri[0], 
            "title": title[0], 
            "user-agent": choices(user_agent, user_agent_weights)[0]
        }
    }
    requests.post(HOST_URI, data=json.dumps(obj), timeout=20)
    print(obj['tenant-name'], "/", obj['entrypoint-name'], ': Sent', obj['value'])

if __name__ == "__main__":
    rating_value = list(range(1,6))
    
    #tenant = os.environ.get('TENANT', 'comune-di-genova')
    tenant = ['comune-di-genova']
    
    #entrypoint = os.environ.get('ENTRYPOINT', 'segnalaci')
    entrypoint = ['segnalaci']
    
    uri_segnalaci_genova = ['https://segnalazioni.comune.genova.it/sensor/posts']
    
    title_segnalaci = ['Tutte le segnalazioni']
    user_agent = [
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.3', 
            'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/43.4', 
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 
            'Mozilla/5.0 (iPhone; CPU iPhone OS 11_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) ', 
            'Version/10.0 Mobile/14E304 Safari/602.1'
    ]
    sleep_min = int(os.environ.get('SLEEP_MIN', 1))
    sleep_max = int(os.environ.get('SLEEP_MAX', 3))

    while True:
        tenant_weights = list(np.random.uniform(0,1,2))
        entrypoint_weights = list(np.random.uniform(0,1,2))

        selected_tenant = tenant[0]
        selected_entrypoint = entrypoint[0]

        send_rating(sleep_min, sleep_max, rating_value, selected_tenant, selected_entrypoint, title_segnalaci, uri_segnalaci_genova, user_agent)